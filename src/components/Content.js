import React, {Component} from 'react';
import cards_ from "../cards.json";
import Presentation from './Presentation'
import Cards from './Cards'

class Content extends React.Component {

    state = {
        cards: cards_, // datos json
    }

    render() {
        return (
            <div id="content" className={''}>
                <Presentation lang={this.props.lang}/>

                <div className="content-right">
                    <div className={'d-flex flex-column align-items-center h5 mt-5 p-3 text-center'}>
                        <p>{this.props.lang === 'en' ? 'Do you want to know more about me?' : '¿Quieres saber más de mí?'}</p>
                        <p>{this.props.lang === 'en' ? 'Just keep scrolling' : 'Sigue bajando'}</p>
                    </div>
                    <div className={"card-content"}>
                        <Cards cards={this.state.cards.filter(card => card.lang === this.props.lang)}
                               lang={this.props.lang}/>
                        <div style={{height: '300px'}} className={'w-100 mt-5 mb-5 pt-5 pb-5'}>
                            <p className={'text-white'}>{this.props.lang === 'en' ? 'Check out my projects on' : 'Mira mis proyectos en'}
                                <a className={'ml-2'} href="https://gitlab.com/gmarsi">GitLab.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Content;
