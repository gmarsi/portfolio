function Stars() {
    const items = []
    for (let i = 0; i < 200; i++) {
        items.push(
            <div key={'star_' + i} className="circle-container">
                <div className="circle"/>
            </div>
        )
    }
    return (<div id="stars" className={'clouds'}>{items}</div>)
}

export default Stars;
