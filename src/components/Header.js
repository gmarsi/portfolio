import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

class Header extends Component {
    render() {
        return (
            <div id="header" className={'d-flex justify-content-between'}>
                <NavLink exact to="/" activeClassName="active"
                         className={'mr-3'}>{this.props.lang === 'en' ? 'Home' : 'Inicio'}</NavLink>
                <div>
                    <NavLink exact to="/cv" activeClassName="active"
                             className={'mr-3'}>{this.props.lang === 'en' ? 'My CV' : 'Mi CV'}</NavLink>
                    <button onClick={this.props.changeLang.bind(this, 'en')}
                            className={this.props.lang === 'en' ? 'active font-weight-bold mr-3' : 'font-weight-bold mr-3'}>EN
                    </button>
                    <button onClick={this.props.changeLang.bind(this, 'es')}
                            className={this.props.lang === 'es' ? 'active font-weight-bold mr-3' : 'font-weight-bold mr-3'}>ES
                    </button>
                </div>
            </div>
        );
    }
};

export default Header;
