import React, {Component} from 'react';

class Presentation extends Component {
    render() {
        return (
            <div className="content-left">
                <div className={'d-flex flex-column'}>
                    <div className={'font-weight-bold h1 mb-5'}>
                        <p>{this.props.lang === 'en' ? "Hi, I'm Marcos Giménez." : "Hola, soy Marcos Giménez."}</p>
                        <p>{this.props.lang === 'en' ? "I'm a web developer," : "Soy desarrollador web,"}</p>
                        <p>{this.props.lang === 'en' ? "an adventure seeker," : "buscador de aventuras"}</p>
                        <p>{this.props.lang === 'en' ? "and a tech and science lover." : "y amante de la ciéncia y la teconolgía."}</p>
                    </div>
                </div>
                <div>
                    <div className={'d-flex'}>
                        <span
                            className={'contact-title'}>{this.props.lang === 'en' ? "Contact with me" : "Contacta conmigo"}</span>
                        <a href="mailto:me@gmarsi.com">me@gmarsi.com</a>
                    </div>
                    <div className={'d-flex'}>
                        <span
                            className={'contact-title'}>{this.props.lang === 'en' ? "Want to call me?" : "¿Quieres llamarme?"}</span>
                        <span>+34 671 91 55 04</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default Presentation;
