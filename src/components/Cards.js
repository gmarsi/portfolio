import React, {Component} from 'react';

class Card extends React.Component {

    cardStyle = {
        top: this.props.top + 'px',
        width: this.props.width + '%',
        title: this.props.title,
        content: this.props.content
    }

    render() {
        return (
            <div className={'g-card'} style={this.cardStyle}>
                <p className="g-card-title">{this.props.title}</p>
                <p className="g-card-content">{this.props.content}</p>
            </div>
        )
    }
}

class Cards extends React.Component {
    render() {
        return this.props.cards.map(card => <Card key={card.id} title={card.title} content={card.content}
                                                  top={card.top} width={card.width}/>)
    }
}

export default Cards;
