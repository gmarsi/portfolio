import React, {Component} from 'react';
import cv_ from "../cv.json";

class Cv extends Component {

    state = {
        cv_data: cv_
    }

    styleTitle = {
        borderBottom: '3px solid #38D996',
        fontSize: '24px',
        color: 'white',
        // width: '50%',
        marginBottom: '3rem',
        paddingBottom: '.5rem'
    }

    styleLabel = {
        fontWeight: 'bold',
        marginRight: '.5rem'
    }

    styleData = {
        color: '#38D996'
    }


    render() {
        const cv_lang = this.state.cv_data.filter(c => c.lang === this.props.lang)
        const cv = cv_lang[0]
        return (

            <div id="cv" className={'container mb-5 pb-5'}>
                <p style={this.styleTitle}>{cv.name}</p>
                <div className="d-flex flex-column">
                    <div className={'d-flex flex-column flex-md-row justify-content-between'}>
                        <p><span
                            style={this.styleLabel}>{this.props.lang === 'en' ? "Current location" : "Ubicación actual"}</span><span
                            style={this.styleData}>{cv.location}</span></p>
                        <p><span
                            style={this.styleLabel}>{this.props.lang === 'en' ? "Contact phone" : "Teléfono de contacto"}</span><span
                            style={this.styleData}>{cv.phone}</span></p>
                    </div>
                    <div className={'d-flex flex-column flex-md-row justify-content-between'}>
                        <p><span
                            style={this.styleLabel}>{this.props.lang === 'en' ? "Birthday" : "Teléfono de contacto"}</span><span
                            style={this.styleData}>{cv.birthday}</span></p>
                        <p><span
                            style={this.styleLabel}>{this.props.lang === 'en' ? "Email" : "Correo electrónico"}</span><span
                            style={this.styleData}>{cv.email}</span></p>
                    </div>
                </div>
                <p style={this.styleTitle}
                   className={'mt-5 col-12 p-0 col-md-6'}>{this.props.lang === 'en' ? "Studies" : "Estudios"}</p>
                <div className={'d-flex flex-column'}>
                    {cv.studies.map((s, k) =>
                        <p key={k}><span style={this.styleLabel}>( {s.from} - {s.to} )</span><span
                            style={this.styleData}>{s.title} | {s.center}, {s.location}</span>
                        </p>)}
                </div>
                <p style={this.styleTitle}
                   className={'mt-5 col-12 p-0 col-md-6'}>{this.props.lang === 'en' ? "Languages" : "Idiomas"}</p>
                {cv.languages.map((l, k) =>
                    <p key={k}>
                        <span style={this.styleLabel}>{l.lang}</span>
                        <span style={this.styleData}>{l.level}</span>
                    </p>
                )}
                <p style={this.styleTitle}
                   className={'mt-5 col-12 p-0 col-md-6'}>{this.props.lang === 'en' ? "Programming Languages" : "Lenguajes de programación"}</p>
                {cv.dev_languages.map((l, k) =>
                    <p key={k}>
                        <span style={this.styleLabel}>{l.lang}</span>
                        {l.frameworks.map((f, k) => <span key={k} className={'mr-1'}
                                                          style={this.styleData}>{f},</span>)}
                    </p>
                )}
            </div>
        )
    }

}

export default Cv;
