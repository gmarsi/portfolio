import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import './background.scss';
import Stars from './components/Stars'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Header from './components/Header'
import Cv from './components/Cv'
import Content from './components/Content'

class Core extends React.Component {

    state = {
        lang: 'en'
    }

    changeLang = (lang) => {
        this.setState({
            lang: lang
        })
    }

    render() {
        return (
            <Router>
                <div id="core">
                    <Stars/>
                    <Header changeLang={this.changeLang} lang={this.state.lang}/>
                    <Switch>
                        <Route path="/cv"><Cv lang={this.state.lang}/></Route>
                        <Route exacte path="/"><Content lang={this.state.lang}/></Route>
                    </Switch>

                </div>
            </Router>
        )
    }
}


ReactDOM.render(
    <Core/>
    ,
    document.getElementById('root')
);

